package com.example.controller;

import com.example.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

/**
 * Created by WIN7 on 2016-09-20.
 */
@Controller
@Slf4j
public class WebSocketController {

    @Autowired
    SimpMessagingTemplate template;

    @MessageMapping("/hello")
    @SendTo("/topic/user")
    User helloUser(User user) throws Exception {

        log.debug(user.toString());

        if( user == null ){
            user = new User();
        }

        user.setAge("199");
        user.setName("이거슨 서버에서 주는거임");

        template.convertAndSend("/topic/user", user);

        return user;
    }


}
