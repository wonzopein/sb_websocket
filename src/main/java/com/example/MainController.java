package com.example;

import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by WIN7 on 2016-09-20.
 */
@Controller
@Slf4j
public class MainController {

    @RequestMapping("/")
    String main(){
        log.debug("yo!");
        return "index";
    }
}
