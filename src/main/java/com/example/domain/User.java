package com.example.domain;

import lombok.Data;

/**
 * Created by WIN7 on 2016-09-20.
 */
@Data
public class User {

    public String name;
    String age;
    String message;

}
