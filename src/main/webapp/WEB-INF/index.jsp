<%--
  Created by IntelliJ IDEA.
  User: WIN7
  Date: 2016-09-20
  Time: 오전 10:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.1.1/sockjs.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>
</head>
<body>
    Hello, Spring Boot.

    <div>
        <div class="icon conn">C</div>
        <div class="icon state">S</div>
    </div>
    <div>
        <input type="text" id="msg">
        <button onclick="send()">쏜다</button>
    </div>


    <script>
        
        var stompClient = null;

        function setConnect(isConnected) {
            if(isConnected){
                $('.icon.conn').addClass('connected');
                $('.icon.state').addClass('connected');
            }else{
                $('.icon').addClass('disconnect');
            }

        }
        
        function blinkState() {
            $('.icon.state').hide().fadeIn();
        }

        function connect() {
            var socket = new SockJS('/kc-websocket');
            
            stompClient = Stomp.over(socket);
            stompClient.connect({},function (frame) {
                setConnect(true);

                stompClient.subscribe('/topic/user', function (user) {
                    console.log(user);
                    blinkState();
                })

            })

        }
        
        function send() {

            var msg = $('#msg').val();

            if(msg.length ==0) {
                alert('메시지를 입력해주세요.');
            }else {
                stompClient.send("/app/hello", {}, JSON.stringify({'message': msg}));
            }
        }

        connect();
        
    </script>

    <style>
        .icon {
            display: inline-block;
            width: 20px;
            height: 20px;
            border: 1px solid #f1f1f1;
            border-radius: 10px;
            background-color: #eee;
            font-size: 0.65em;
            text-align: center;
        }

        .icon.state.connected {
            background-color: #6d73ff;
        }

        .icon.conn.connected {
            background-color: #19c800;
        }
        .icon.conn.disconnect {
            background-color: #ee311e;
        }
    </style>
</body>
</html>
